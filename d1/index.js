/*-----------index.js----------------*/
  /*Using DOM

		before, we are using the following codes in terms of selecting elements inside the html file
*/
// document refers to the whole webpage and the "getElementById", "getElementByClassName", and getElementByClassName select the element with the same id/class/tag as its arguments
/*document.getElementById('txt-first-name');
document.getElementByClassName('txt-last-name');
document.getElementByTagName('input');*/

// querySelector replaces the three "getElement" selectors and makes use of css format in terms of selecting the elements inside the html as its arguments (# - id, . - class, tagName - tag)
document.querySelector('#txt-first-name');


// Event Listeners
// events are all interactions of the user to our webpage; such examples are clicking, hovering, reloading, keypress/keyup( typing );
const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');
// to perform an action when an event triggers, first you have to listen to it

/*
	the function used is the  addEventListener - allows a block of  codes to listen to an event for them to be executed
		addEventListener - takes two arguments: 
			1 - a string that identifies the event to which the codes will listen; 
			2 - a function that the listener will execute once the specified event is triggered
*/
txtFirstName.addEventListener('keyup', (event)=>{
	// innerHTML - this allows the element to record/duplicate the value of the selected variable (txtFirstName.value)
	// .value is needed since without it, the .innerHTML will only record what type of element the target variable is inside the HTML document instead of getting its value
	spanFullName.innerHTML = txtFirstName.value;
})

txtFirstName.addEventListener('keyup', (event)=>{
	console.log(event.target);
	console.log(event.target.value);
})

txtLastName.addEventListener('keyup', (event)=>{
	// innerHTML - this allows the element to record/duplicate the value of the selected variable (txtFirstName.value)
	// .value is needed since without it, the .innerHTML will only record what type of element the target variable is inside the HTML document instead of getting its value
	spanFullName.innerHTML = " " + txtLastName.value;
})

txtLastName.addEventListener('keyup', (event)=>{
	console.log(event.target);
	console.log(event.target.value);
})